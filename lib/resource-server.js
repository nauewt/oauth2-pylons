var Promise = require('bluebird')
  , util = require('util')
  , url = require('url')
  , _ = require('lodash')
  , request = require('request')
  , ursa = require('ursa')
  , dump = function (obj) { console.log(util.inspect(obj, { depth: null, colors: true })); };


var ResourceServer = function (options) {
  this.debug = options.debug
    ? function () {
        Array.prototype.slice.apply(arguments).map(dump);
        console.log('');
      }
    : function () {};

  this.realm = null;
  this.tokenValidationUri = null;
  this.ticketUri = null;
  this.scopePrefix = null;
  this.scopes = null;

  this.set(options);
};


ResourceServer.prototype.set = function (options) {
  options = _.extend({
    realm: null,
    tokenValidationUri: null,
    ticketUri: null,
    scopePrefix: '',
    scopes: null
  }, options);


  if (options.realm) {
    this.realm = options.realm;
  }

  if (options.tokenValidationUri) {
    this.tokenValidationUri = options.tokenValidationUri;
  }

  if (options.ticketUri) {
    this.ticketUri = options.ticketUri;
  }

  if (options.scopePrefix) {
    this.scopePrefix = options.scopePrefix;
  }

  if (options.scopes) {
    this.setScopes(options.scopes);
  }
};


ResourceServer.prototype.setScopes = function (scopes) {
  if (_.isObject(scopes) && _.every(scopes, function (scope, path) {
    return _.isString(path) && (_.isString(scope) || _.isArray(scope));
  })) {
    for (var path in scopes) {
      if (scopes.hasOwnProperty(path)) {
        scopes[path] = _.map(
          _.isArray(scopes[path]) ? scopes[path] : scopes[path].split(/\s+/),
          function (scope) {
            return (this.scopePrefix ? this.scopePrefix + '/' : '') + scope;
          }.bind(this)
        );
      }
    }
    this.scopes = scopes;
  } else {
    throw Error('Path scopes must be an object with path (string regex) as keys and scopes as values.');
  }
};;


ResourceServer.prototype.getAuthenticationMiddleware = function (options) {
  // last second option setting
  if (options) this.set(options);

  if (!(this.realm && this.tokenValidationUri && this.scopes)) {
    throw Error('Realm, validation URI, and scopes are all required.');
  }

  // Make local copies of closure vars
  var realm = this.realm.slice()
    , tokenValidationUri = this.tokenValidationUri.slice()
    , scopes = _.cloneDeep(this.scopes)
    , getScopes = function (reqPath) {
        for (var path in scopes) {
          if (scopes.hasOwnProperty(path)) {
            if ((path === reqPath) || (new RegExp('^' + path)).test(reqPath)) {
              return scopes[path];
            }
          }
        }
        return '';
      };

  this.debug('[oauth2-pylons] ResourceServer::getAuthenticationMiddleware - OAuth2 Resource Server authentication active!');
  this.debug('[oauth2-pylons] ResourceServer::getAuthenticationMiddleware - Route -> Scope Map:', scopes);

  // Actual middleware function
  return function (req, res, next) {
    var path = url.parse(req.originalUrl)
      , scopes = getScopes(path.pathname)
      , wwwAuthenticate = 'Bearer realm="' + realm + '" scope="' + scopes + '"'
      , authorization = req.headers['Authorization'] || req.headers['authorization']
      , bearerPattern = /Bearer\s+(.+)$/i;

    this.debug('[oauth2-pylons] ResourceServer::auth - Checking Authorization: ' + wwwAuthenticate);
    this.debug('[oauth2-pylons] ResourceServer::auth - Authorization: ' + authorization);

    // The authorization header must be present and must be the right format
    if (bearerPattern.test(authorization)) {
      // BEARER TOKEN AUTHORIZATION
      this.debug('[oauth2-pylons] ResourceServer::auth - Bearer token, validating with oauth server:');

      var token = authorization.match(bearerPattern).pop().trim();

      request.post({
        url: tokenValidationUri,
        headers: {
          'Authorization': 'Bearer ' + token,
          'Scope': scopes
        }
      }, function (err, response, body) {
        if (err || response.statusCode >= 500) {
          // Fatal error making request
          this.debug('[oauth2-pylons] ResourceServer::auth - Access validate error: ' + (err || '\nBody: ' + body));
          res.set('WWW-Authenticate', wwwAuthenticate);
          res.send(500);
          return res.end();
        }

        if (response.statusCode >= 400 && response.statusCode < 500) {
          // Token validation failed. Base our response on what was returned by the server
          this.debug('[oauth2-pylons] ResourceServer::auth - Access validate failed: (' + response.statusCode + ') ');
          this.debug('[oauth2-pylons] ResourceServer::auth', body);

          switch (response.statusCode) {
            case 400: // Bad request
              wwwAuthenticate += ' error="invalid_request"';
              break;
            case 401: // Token is expired, revoked, etc
              wwwAuthenticate += ' error="invalid_token"';
              break;
            case 403: // Token is ok, but not valid for the given scope
              wwwAuthenticate += ' error="insufficient_scope"';
              break;
          }

          res.set('WWW-Authenticate', wwwAuthenticate);
          res.send(response.statusCode);
          return res.end();
        }

        // Token is valid for given scope! Continue to resource
        this.debug('[oauth2-pylons] ResourceServer::auth - Authorized, proceed to resource.');

        req.oauthAccess = (typeof body === 'string') ? JSON.parse(body) : body;
        if (req.session) {
          req.session.oauthAccess = req.oauthAccess;
        }
        next();
      });

    } else {
      // No token or malformed request. Do not supply an error code (per RFC)
      this.debug('[oauth2-pylons] ResourceServer::auth - Bad/no token header provided');
      res.set('WWW-Authenticate', wwwAuthenticate);
      res.send(401);
      return res.end();
    }
  };
};


// URL Ticket methods

ResourceServer.prototype.getTicket = function (user_id) {
  return new Promise(function (resolve, reject) {
    if (!this.ticketUri) {
      reject(Error('Ticket URI is required.'));
    }

    var ticketUri = this.ticketUri + '/' + user_id;

    request.get({
      url: ticketUri
    }, function (err, response, body) {
      if (err || response.statusCode >= 400) {
        // Fatal error making request
        this.debug('[oauth2-pylons] ResourceServer::getTicket - Error generating ticket: ' + (err || '\nBody: ' + body));
        reject(Error('Could not generate ticket'));
      }

      var ticket = (typeof body === 'string') ? JSON.parse(body) : body;

      this.debug('[oauth2-pylons] ResourceServer::getTicket - Received ticket for ' + user_id, ticket);

      resolve(ticket);
    });



  });
};


ResourceServer.prototype.getTicketUri = function (uri, user_id) {
  return this.getTicket(user_id)
    .then(function (ticket) {
      if (!(ticket && ticket.ticket)) {
        this.debug('[oauth2-pylons] ResourceServer::getTicketUri - Error generating ticket URI: ' + (err || ticket));
        throw Error('Could not generate ticket URI');
      }

      var newUri = uri + (/\?\w+\=/.test(uri) ? '&' : '?') + '_ticket=' + ticket.ticket;

      return newUri;
    }.bind(this));
};


ResourceServer.prototype.getTicketMiddleware = function (options) {
  // last second option setting
  if (options) this.set(options);

  if (!this.ticketUri) {
    throw Error('Ticket URI is required.');
  }

  // Make local copies of closure vars
  var ticketUri = this.ticketUri.slice();

  this.debug('[oauth2-pylons] ResourceServer::getTicketMiddleware - OAuth2 Resource Server ticket validation active!');

  // Actual middleware function
  return function (req, res, next) {
    req.ticket = null;
    var ticket = req.query._ticket || null;

    if (!ticket) return next(); // No ticket, don't care

    // Ticket present, check it
    this.debug('[oauth2-pylons] ResourceServer::ticket - Checking Ticket: ' + ticket);
    request.post({
      url: ticketUri,
      form: {
        ticket: ticket
      }
    }, function (err, response, body) {
      if (err || response.statusCode >= 400) {
        // Fatal error making request
        this.debug('[oauth2-pylons] ResourceServer::ticket - Ticket validate error: ' + (err || response.statusCode));
        res.send(404);
        return res.end();
      }

      this.debug('[oauth2-pylons] ResourceServer::ticket - Ticket valid, proceed to resource.');
      req.ticket = (typeof body === 'string') ? JSON.parse(body) : body;
      next();
    });
  };
};


// Static convenience
ResourceServer.authenticationMiddlware = function (options) {
  return (new ResourceServer(options)).getAuthenticationMiddleware(options);
};

ResourceServer.ticketMiddlware = function (options) {
  return (new ResourceServer()).getTicketMiddleware(options);
};

ResourceServer.attach = function (app, options) {
  app.use(ResourceServer.authenticationMiddlware(options));

  if (options && options.tickets) {
    delete options.tickets;
    app.use(ResourceServer.ticketMiddlware(options));
  }
};


module.exports.ResourceServer = ResourceServer;
