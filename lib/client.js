var util = require('util')
  , url = require('url')
  , _ = require('lodash')
  , request = require('request')
  , OAuth2 = require('simple-oauth2')
  , dump = function (obj) { console.log(util.inspect(obj, { depth: null, colors: true })); };


var Client = function (options) {
  this.debug = options.debug
    ? function () {
        Array.prototype.slice.apply(arguments).map(dump);
        console.log('');
      }
    : function () {};

  this.oAuthClientConfig = {
    clientID: null,
    clientSecret: null,
    site: null,
    authorizationPath: null,
    tokenPath: null
  }

  this.pathConfig = {
    clientUri: null,
    callbackPath: null,
    completePath: null,
    errorPath: null
  }

  // Required for use by Simple-Oauth 2 node client
  this.oAuthAuthorizeConfig = {
    redirect_uri: null,
    scope: null,
    state: null
  }

  this.oAuthClient = null
  this.oAuthAuthorizeUri = null

  this.auth = {
    code: null,
    token: null
  }

  this.set(options)
}


Client.prototype.set = function (options) {
  options = _.extend({
    // config
    clientId: null,
    clientSecret: null,
    clientUri: null,
    oauth2Uri: 'https://api.nau.edu/oauth2',
    authorizationPath: '/auth',
    tokenPath: '/token',
    ticketPath: '/ticket',
    // auth config
    callbackPath: null,
    completePath: null,
    errorPath: null,
    scope: null,
    state: null
  }, options)


  if (options.clientId) {
    this.oAuthClientConfig.clientID = options.clientId
  }

  if (options.clientSecret) {
    this.oAuthClientConfig.clientSecret = options.clientSecret
  }

  if (options.oauth2Uri) {
    this.oAuthClientConfig.site = options.oauth2Uri
  }

  if (options.authorizationPath) {
    this.oAuthClientConfig.authorizationPath = options.authorizationPath
  }

  if (options.tokenPath) {
    this.oAuthClientConfig.tokenPath = options.tokenPath
  }

  if (options.clientUri) {
    this.pathConfig.clientUri = options.clientUri
  }

  if (options.callbackPath) {
    this.pathConfig.callbackPath = options.callbackPath
  }

  if (options.completePath) {
    this.pathConfig.completePath = options.completePath
  }

  if (options.errorPath) {
    this.pathConfig.errorPath = options.errorPath
  }

  if (options.scope) {
    this.oAuthAuthorizeConfig.scope = options.scope
  }

  if (options.state) {
    this.oAuthAuthorizeConfig.state = options.state
  }

  if (!this.pathConfig.errorPath && this.pathConfig.completePath) {
    this.pathConfig.errorPath = this.pathConfig.completePath
  }

  if (!this.oAuthAuthorizeConfig.redirect_uri && (this.pathConfig.clientUri && this.pathConfig.callbackPath)) {
    this.oAuthAuthorizeConfig.redirect_uri = this.pathConfig.clientUri + this.pathConfig.callbackPath
  }


  this.oAuthClient = OAuth2(this.oAuthClientConfig)
  // Returns the Oauth authorization url
  this.oAuthAuthorizeUri = this.oAuthClient.AuthCode.authorizeURL(this.oAuthAuthorizeConfig)
}


/**
 * Provides an Express handler for authorizing the user.
 * @param  {Object} options
 * @return {Function}
 */
Client.prototype.getAuthorizationRoute = function (options) {
  if (options) this.set(options)

  return function (req, res) {
    this.debug('[oauth2-pylons] Client::auth - Authorization Forward')
    res.redirect(this.oAuthAuthorizeUri)
  }.bind(this)
}


/**
 * Provides an Express handler for the callback request from the OAuth2 server.
 * @param  {Object} options
 * @return {Function}
 */
Client.prototype.getCallbackRoute = function (options) {
  if (options) this.set(options)

  return function (req, res) {
    this.debug('[oauth2-pylons] Client::callback - Authorization Callback')

    this.auth.code = req.param('code')
    if (req.session) req.session.auth = this.auth

    this.debug('[oauth2-pylons] Client::callback - Auth code received: ' + this.auth.code)

    this.oAuthClient.AuthCode.getToken({
      code: this.auth.code,
      client_secret: this.oAuthClientConfig.clientSecret,
      redirect_uri: this.oAuthAuthorizeConfig.redirect_uri
    }, function (error, result) {
      if (error) {
        this.debug('[oauth2-pylons] Client::callback - Access Token Error', error.message.message)
        res.redirect(this.pathConfig.clientUri + this.pathConfig.errorPath + '?error=' + error.message.message)
      } else {
        var token = this.oAuthClient.AccessToken.create(result)

        this.auth.token = token.token ? token.token : token
        if (req.session) req.session.auth = this.auth

        this.debug('[oauth2-pylons] Client::callback - Access token received: ', this.auth.token)
        res.redirect(this.pathConfig.clientUri + this.pathConfig.completePath)
      }
    }.bind(this))
  }.bind(this)
}


/**
 * Automatically attaches client routes and callbacks to a given Express app.
 * @param  {Object} Express app instance to attach to
 * @param  {Object} options
 * @return {Object} Client instance
 */
Client.attach = function (app, options) {
  var callbackRoute = options.callbackRoute || options.callbackPath || '/callback'
    , authorizeRoute = options.authorizeRoute || options.authorizePath || '/authorize'

  options = options || {}
  options.callbackPath = callbackRoute
  options.authorizePath = authorizeRoute
  options.completePath = options.completeRoute || options.completePath || '/'
  options.errorPath = options.errorRoute || options.errorPath || '/'

  var client = new Client(options)

  client.debug('[oauth2-pylons] Client::attach - Attaching authorize redirect to ' + authorizeRoute)
  client.debug('[oauth2-pylons] Client::attach - Attaching authorize callback to ' + callbackRoute)

  app.all(callbackRoute, client.getCallbackRoute())
  app.all(authorizeRoute, client.getAuthorizationRoute())

  return client
}


module.exports.Client = Client
