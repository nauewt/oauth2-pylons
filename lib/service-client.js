var Promise = require('bluebird')
  , util = require('util')
  , url = require('url')
  , _ = require('lodash')
  , request = require('request')
  , ursa = require('ursa')
  , dump = function (obj) { console.log(util.inspect(obj, { depth: null, colors: true })); };


var ServiceClient = function (options) {
  this.debug = options.debug
    ? function () {
        Array.prototype.slice.apply(arguments).map(dump);
        console.log('');
      }
    : function () {};

  this.oAuthClientConfig = {
    clientId: null,
    clientSecret: null,
    clientKey: null,
    site: null,
    authorizationPath: null,
    tokenPath: null,
    ticketPath: null,
    alwaysReauth: false
  };

  this.auth = {
    user_id: null,
    code: null,
    accessToken: null,
    refreshToken: null,
    expiresIn: null,
    expiresAt: null
  };

  this.set(options);
};


ServiceClient.prototype.set = function (options) {
  options = _.extend({
    clientId: null,
    clientSecret: null,
    clientKey: null,
    oauth2Uri: 'https://api.nau.edu/oauth2',
    authorizationPath: '/auth',
    tokenPath: '/token',
    ticketPath: '/ticket',
    user_id: null,
    alwaysReauth: false
  }, options);


  if (options.clientId) {
    this.oAuthClientConfig.clientId = options.clientId;
  }

  if (options.clientSecret) {
    this.oAuthClientConfig.clientSecret = options.clientSecret;
  }

  if (options.clientKey) {
    this.oAuthClientConfig.clientKey = ursa.createPrivateKey(options.clientKey);
  }

  if (options.oauth2Uri) {
    this.oAuthClientConfig.site = options.oauth2Uri;
  }

  if (options.authorizationPath) {
    this.oAuthClientConfig.authorizationPath = options.authorizationPath;
  }

  if (options.tokenPath) {
    this.oAuthClientConfig.tokenPath = options.tokenPath;
  }

  if (options.ticketPath) {
    this.oAuthClientConfig.ticketPath = options.ticketPath;
  }

  if (options.user_id || options.uid || options.userId) {
    this.auth.user_id = options.user_id || options.uid || options.userId;
  }

  if (options.alwaysReauth) {
    this.oAuthClientConfig.alwaysReauth = !!options.alwaysReauth;
  }
};


ServiceClient.prototype.validateOptions = function () {
  if (!this.oAuthClientConfig.clientId)          throw Error('clientId is required.');
  if (!this.oAuthClientConfig.clientSecret)      throw Error('clientSecret is required.');
  if (!this.oAuthClientConfig.clientKey)         throw Error('clientKey is required.');
  if (!this.oAuthClientConfig.site)              throw Error('oauth2Uri is required.');
  if (!this.oAuthClientConfig.tokenPath)         throw Error('tokenPath is required.');
  if (!this.oAuthClientConfig.authorizationPath) throw Error('authorizationPath is required.');
  if (!this.auth.user_id)                        throw Error('user_id is required.');
};


ServiceClient.prototype.isAuthorized = function () {
  return this.auth.code
    && this.auth.code.isFulfilled
    && (this.auth.code.isFulfilled() || this.auth.code.isPending());
};


ServiceClient.prototype.hasAccess = function () {
  return this.auth.accessToken
    && this.auth.accessToken.isFulfilled
    && (this.auth.accessToken.isFulfilled() || this.auth.accessToken.isPending());
};


ServiceClient.prototype.isExpired = function () {
  return this.hasAccess()
    && !this.auth.accessToken.isPending()
    && this.auth.expiresAt
    && (Date.now() > this.auth.expiresAt);
};


ServiceClient.prototype.shouldReauthWhenExpired = function () {
  return this.isExpired()
    && !this.auth.code.isPending()
    && this.oAuthClientConfig.alwaysReauth;
};


ServiceClient.prototype.signMessage = function (message) {
  if (!this.oAuthClientConfig.clientKey) throw Error('Private key not set');

  if (!_.isString(message)) {
    message = JSON.stringify(message);
  }

  return {
    encoded: Buffer(message, 'utf8').toString('base64'),
    signature: this.oAuthClientConfig.clientKey.hashAndSign('sha256', message, 'utf8', 'base64')
  };
};


ServiceClient.prototype.getAuthorizationHeader = function (options) {
  if (options) this.set(options);
  this.validateOptions();

  var message = this.signMessage({ user_id: this.auth.user_id });
  return 'Service ' + this.oAuthClientConfig.clientId + ' ' + message.encoded + ' ' + message.signature;
};


/**
 * Authorizes this client with the OAuth2 server
 * @param  {Object} options Options as in constructor
 * @return {Promise}        Promise which is passed the string code on resolve
 */
ServiceClient.prototype.getAuthCode = function (options) {
  // Refresh the authorization if we are not authorized
  // -or- if access has expired but we should always reauth instead of refresh
  if (!this.isAuthorized() || this.shouldReauthWhenExpired()) {
    this.debug('[oauth2-pylons] ServiceClient::getAuthCode creating promise');

    this.auth.code = new Promise(function (resolve, reject) {
      if (options) this.set(options);
      this.validateOptions();

      request.get({
        url: this.oAuthClientConfig.site + this.oAuthClientConfig.authorizationPath,
        headers: {
          'Authorization': this.getAuthorizationHeader()
        },
        json: true
      }, function (err, response, body) {
        this.debug('[oauth2-pylons] ServiceClient::getAuthCode', err, body);

        if (err) {
          return reject(err);
        }

        if (body.code) {
          return resolve(body.code);
        } else {
          return reject(Error('No auth code in response from server'));
        }
      }.bind(this));
    }.bind(this));
  }

  return this.auth.code;
};


/**
 * Gets a current access token from the OAuth2 server. Authorizes the client if needed.
 * @param  {Object} options Options as in constructor
 * @return {Promise}        Promise which is passed an object containing the access token on resolve
 */
ServiceClient.prototype.getAccessToken = function (options) {
  if (!this.hasAccess() || this.isExpired()) {
    this.auth.accessToken = this.getAuthCode(options)
      .then(function (authCode) {
        this.debug('[oauth2-pylons] ServiceClient::getAccessToken creating promise', authCode);

        return new Promise(function (resolve, reject) {
          if (options) this.set(options)
          this.validateOptions()

          request.post({
            url: this.oAuthClientConfig.site + this.oAuthClientConfig.tokenPath,
            form: {
              grant_type: this.isExpired() ? 'refresh_token' : 'authorization_code',
              client_id: this.oAuthClientConfig.clientId,
              client_secret: this.oAuthClientConfig.clientSecret,
              code: authCode,
              refresh_token: this.auth.refreshToken
            },
            json: true
          }, function (err, response, body) {
            this.debug('[oauth2-pylons] ServiceClient::getAccessToken', err, body);

            if (err) {
              return reject(err);
            }

            if (body.access_token) {
              if (body.refresh_token) {
                this.auth.refreshToken = body.refresh_token;
              }

              if (body.expires_in) {
                body.expires_at = Date.parse(body.expires) - 60000; // todo: lolworkaround

                this.auth.expiresIn = body.expires_in;
                this.auth.expiresAt = body.expires_at;
              }

              return resolve(body);
            } else {
              return reject(Error('No access token in response from server'));
            }
          }.bind(this));
        }.bind(this));
      }.bind(this));
  }

  return this.auth.accessToken;
};


/**
 * Alias for getAccessToken, generally this is all you would need to call from client code.
 * @param  {Object} options Options as in constructor
 * @return {Promise}        Promise which is passed an object containing the access token on resolve
 */
ServiceClient.prototype.authorize = function (options) {
  return this.getAccessToken(options);
};


/**
 * Gets a ticket for each user id in the given array.
 * @param  {Array<string>} user_ids
 * @return {Promise}
 */
ServiceClient.prototype.getTickets = function (user_ids) {
  return this.getAccessToken()
    .then(function (access) {
      return new Promise(function (resolve, reject) {
        if (!access) {
          reject(Error('Missing or bad access token.'));
        }

        request.post({
          uri: this.oAuthClientConfig.site + this.oAuthClientConfig.ticketPath,
          headers: {
            'Authorization': 'Bearer ' + access.access_token
          },
          body: user_ids,
          json: true
        }, function (err, response, body) {
          this.debug('[oauth2-pylons] ServiceClient::getTickets', err, body);

          if (err || response.statusCode !== 200) {
            reject(err || (body && body.error && Error(body.error)) || (body && Error(body)));
          }

          resolve(body);
        }.bind(this));
      }.bind(this));
    }.bind(this));
};


module.exports.ServiceClient = ServiceClient;
